﻿/// API para integração de PDI com NodeJS
// Desenvolvido por Bix Tecnologia


const express = require('express'); //require "importa" as dependecias. 
const app = express();              //chamamos a aplicação/core da api
const bodyParser = require('body-parser');
const port = 3000; //porta padrão
const mysql = require('mysql');
var cors = require('cors');
const subprocess = require('child_process')

//configurando o body parser para pegar POSTS mais tarde
app.use(bodyParser.urlencoded({ extended: true })); //declara a funcao body parser para "entender" um comando url
app.use(bodyParser.json());   //declara a funcao body parser para "entender" uma mensagem json


//definindo as rotas p/ responder o Get com "Funcionando"
const router = express.Router();    //Classe Router; 
router.get('/', (req, res) => res.send( 'Hello Pentahista!'));
// onde req são dados da requisição(q a web envia), parametros, token de autenticacao.. etc
// ja o res eh utilizado pra enviar a resposta pro usuario/solicitante.
// neste caso esta enviando o "Funcionando"

app.use('/', router);   //app.use é um middleware - uma funcao entre uma requisicao http e uma resposta.
console.log('NodeJS funcionando!');

//inicia o servidor
app.listen(port);   //port esta definida com 3000, é a porta que iremos "ouvir" com nossa API.



app.post('/inserir', function(req, res) { // que será usada pela página para enviar os dados (localhost:3000/inserir) 
    console.log('Dados que serão inseridos:');
    console.log(req.body);
    var campos = { // captura os dados que foram enviados pelo formulário da página 
        nome : req.body.nome,
        idade : req.body.idade,
        email : req.body.email,
        empresa : req.body.empresa
    };
    var dados = { // captura os dados que foram enviados pelo formulário da página 
        pdi : req.body.pdi,
        ktj : req.body.ktj
    };

    // função para executar um comando SQL, passando como parâmetros um json com os campos da tabela
    execSQLTabelaPessoas(campos, dados, res); 

});


// funcao que recebe um json os valores e insere os dados na tabela pessoas
function execSQLTabelaPessoas( campos, dados, res){
    // define os dados de acesso ao banco. Em um ambiente real, estes dados não poderiam ficar expostos 
    const connection = mysql.createConnection({ 
        host     : 'localhost',
        port     : '3306',
        user     : 'pentaho',
        password : 'day',
        database : 'pentaho'
    });

    // executa o sql de 'insert' no banco
    connection.query('INSERT INTO pentaho.pessoas SET ?', campos, function(error, results, fields){
        if(error) 
            res.json(error); // em caso de erro, devolve o erro para o usuário
        else
            console.log('Inseriu no banco');
            console.log('------------------');
            var id = results.insertId;  // id da linha que acabou de ser inserida no banco

            // finalizada a inserção dos dados do formulário no banco, o PDI pode ser chamado para começar a ETL
            execPDI(id , 
                    dados.pdi,  // caminho para o PDI, recebido através do formulário web
                    dados.ktj,  // caminho para o arquivo ktj, recebido através do formulário web
                    res);
            
        connection.end(); // fecha a conexão com o banco
    });
};


// função que recebe o id da linha do banco de dados e executa a ETL em PDI
function execPDI(id, caminhoPDI, caminhoKTJ, res){
  //constroi o comando para executar o PDI. Este comando pode ser executado dentro do seu cmd, para teste
  var comando = caminhoPDI +'\\Kitchen.bat /file:"'+caminhoKTJ+ '" /param:"id='+id+'" /level:Basic'; 
  console.log(comando);

  // usando uma biblioteca específica, é executado aquele comando como se estivesse dentro de um cmd
  const exec = subprocess.exec; 
  var pdi = exec(comando, // executa o comando definido acima
    (error, stdout, stderr) => {
         console.log(`${stdout}`); 
         console.log(`${stderr}`); 
         if (error !== null) {
             console.log(`exec error: ${error}`);
             res.send('Erro na execução');   // em caso de erro, devolve o erro para o usuário
         } else {
            console.log('Finalizando a execução desta requisicao');
            console.log('------------------');
            res.send(`Valores gravados com sucesso.`);  // retorna uma mensagem de sucesso para o usuário
        }
     });
};