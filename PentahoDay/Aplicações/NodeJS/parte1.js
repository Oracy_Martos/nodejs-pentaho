﻿/// API para integração de PDI com NodeJS
// Desenvolvido por Bix Tecnologia

const express = require('express'); //require é resposavel por importar as dependencias 
const app = express();              //chamamos a aplicação/core da api
const bodyParser = require('body-parser');
const port = 3000; //porta padrão
const mysql = require('mysql');
var cors = require('cors');
const subprocess = require('child_process')

//configurando o body parser para receber POSTS futuros
app.use(bodyParser.urlencoded({ extended: true })); //declara a funcao body parser para tratar de comandos url
app.use(bodyParser.json());   //declara a funcao body parser para tratar mensagens do tipo json


//definindo as rotas p/ tratar requisições Get
const router = express.Router();    //Classe Router; 
router.get('/', (req, res) => res.send( 'Hello Pentahista!'));
// onde req são dados da requisição como parametros, token de autenticação.. etc
// ja o res é utilizado para enviar a resposta pro usuario/solicitante.
// neste caso esta enviando o 'Hello Pentahista!'

app.use('/', router);   //app.use é um middleware - uma funcao entre uma requisicao http e uma resposta.
console.log('NodeJS funcionando!');

//inicia o servidor
app.listen(port);   //port esta definida com 3000, é a porta que iremos "ouvir" com nossa API.