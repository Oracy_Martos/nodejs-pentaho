﻿/// API para integração de PDI com NodeJS
// Desenvolvido por Bix Tecnologia


const express = require('express'); //require é resposavel por importar as dependencias 
const app = express();              //chamamos a aplicação/core da api
const bodyParser = require('body-parser');
const port = 3000; //porta padrão
const mysql = require('mysql');
var cors = require('cors');
const subprocess = require('child_process')

//configurando o body parser para receber POSTS futuros
app.use(bodyParser.urlencoded({ extended: true })); //declara a funcao body parser para tratar de comandos url
app.use(bodyParser.json());   //declara a funcao body parser para tratar mensagens do tipo json


//definindo as rotas p/ tratar requisições Get
const router = express.Router();    //Classe Router; 
router.get('/', (req, res) => res.send( 'Hello Pentahista!'));
// onde req são dados da requisição como parametros, token de autenticação.. etc
// ja o res é utilizado para enviar a resposta pro usuario/solicitante.
// neste caso esta enviando o 'Hello Pentahista!'

app.use('/', router);   //app.use é um middleware - uma funcao entre uma requisicao http e uma resposta.
console.log('NodeJS funcionando!');

//inicia o servidor
app.listen(port);   //port esta definida com 3000, é a porta que iremos "ouvir" com nossa API.


//define a tratativa da requisição POST
app.post('/inserir', function(req, res) { // define o endereço usada pela página para enviar os dados (localhost:3000/inserir) 
    console.log('Dados que serão inseridos:');
    console.log(req.body);
    var campos = { // captura os dados que foram enviados pelo formulário da página 
        nome : req.body.nome,
        idade : req.body.idade,
        email : req.body.email,
        empresa : req.body.empresa
    };
    var dados = { // captura os dados que foram enviados pelo formulário da página 
        pdi : req.body.pdi,
        ktj : req.body.ktj
    };

    // função para executar um comando SQL, passando como parâmetros um json com os campos da tabela
    execSQLTabelaPessoas(campos, dados, res); 

});


// funcao que recebe um json os valores e insere os dados na tabela pessoas
function execSQLTabelaPessoas( campos, dados, res){
    // define os dados de acesso ao banco. Em um ambiente real, estes dados não poderiam ficar expostos 
    const connection = mysql.createConnection({ 
        host     : 'localhost   ',
        port     : '3307',
        user     : 'pentaho',
        password : 'day',
        database : 'pentaho'
    });

    // executa o sql de 'insert' no banco
    connection.query('INSERT INTO pentaho.pessoas SET ?', campos, function(error, results, fields){
        if(error) 
            res.json(error); // em caso de erro, devolve o erro para o usuário
        else
            console.log('Inseriu no banco');
            console.log('------------------');
            var id = results.insertId;  // id da linha que acabou de ser inserida no banco
            console.log('Inseriu no banco os dados com o id ' + id);	
            res.send(`Valores gravados com sucesso com o id ` + id); 
        connection.end(); // fecha a conexão com o banco
    });
};