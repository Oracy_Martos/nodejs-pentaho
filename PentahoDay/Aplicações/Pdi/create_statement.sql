﻿CREATE DATABASE `pentaho` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

use pentaho;

CREATE TABLE `pessoas` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `nome` char(40) NOT NULL,
  `idade` char(3) NOT NULL,
  `email` char(50) NOT NULL,
  `empresa` char(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3554 DEFAULT CHARSET=utf8mb4;

CREATE TABLE `pessoas_empresas` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `empresa` char(40) NOT NULL,
  `quantidade` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_pessoas_empresas_lookup` (`empresa`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4;
