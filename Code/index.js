const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const port = 3000
const mysql = require('mysql')
let cors = require('cors')
const subprocess = require('child_process')

app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())

const router = express.Router()
router.get('/', (req, res) => res.send('Working'))


app.use('/', router)

app.post('/inserir', (req, res) => {
    console.log('Inserted')
    console.log(req.body)
    let campos = {
        nome : req.body.nome,
        idade : req.body.idade,
        empresa : req.body.empresa,
        email : req.body.email
    }
    let dados = {
        pdi : req.body.pdi,
        kdj : req.body.kdj
    }

    // res.send(fields)
    execSQLTabelaPessoas(campos, dados, res)
})

execSQLTabelaPessoas = (campos, dados, res) => {
    const connection = mysql.createConnection({
        host: 'localhost',
        port: '3306',
        user: 'root',
        // password: 'positivo',
        database: 'pentaho'
    })

    connection.query('INSERT INTO pentaho.pessoas SET ?', campos, function(error, results, fields){
        if(error) 
            res.json(error)
        else
            console.log('Inseriu no banco')
            console.log('------------------')
            var id = results.insertId 
            console.log('Inseriu no banco os dados com o id ' + id)	
            res.send(`Valores gravados com sucesso com o id ` + id)
            execPDI(id , 
                   dados.pdi,  // caminho para o PDI, recebido através do formulário web
                 dados.ktj,  // caminho para o arquivo ktj, recebido através do formulário web
                   res)

        connection.end()// fecha a conexão com o banco
    })
}

const execPDI = (id, caminhoPDI, caminhoKTJ, res) => {
    let comando = caminhoPDI +'\\Kitchen.bat /file:"'+caminhoKTJ+ ' "/param:id='+id+'" /level:Basic' 
     console.log(comando)
       const exec = subprocess.exec 
     let pdi = exec(comando, 
       (error, stdout, stderr) => {
            console.log(`${stdout}`) 
            if (error !== null) {
                console.log(`exec error: ${error}`)
                res.send('Erro na execução')   
            } else {
               console.log('Finalizando a execução desta requisicao')
               console.log('------------------')
               res.send(`Valores gravados com sucesso.`)  
           }
        })
    }


console.log('It\'s working')
app.listen(port)